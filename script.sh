#!/bin/sh


mkdir -p processed # Creates a processed folder

filenames=''
while IFS='' read -r imageFile || [[ -n "$imageFile" ]]; do
	echo "Processing $imageFile"
	extension=${imageFile##*.} # Get the extension
	filename=${imageFile%.*} # Get the file name
	convert "raw/$imageFile" -resize 25% -crop 150x150+50! "processed/$imageFile"
	filenames="$filenames processed/$imageFile"
done < "$1"

convert -delay 50 $filenames -loop 0  "processed/animated.gif"
cp "processed/animated.gif" "landing page/"

echo "Done"